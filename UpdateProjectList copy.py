#!/usr/bin/python

import argparse
import os
import sys
import yaml

rootElement = "projects"
yamlContent = None
endPoint = None
defaultEndpoint = "bitbk"

def createEmptyYaml(fileToSave):
    with open(fileToSave, 'w') as yamlFile:
        content = "projects:"
        yamlFile.write(content) 

def loadYaml():
    fileToLoad = None
    jenkinsUrl = os.environ['JENKINS_URL']
    cbsProdUrl = "http://cbs.kubemea.science.roche.com/"
    if jenkinsUrl == cbsProdUrl:
        fileToLoad = "projects.yaml"
    else:
        fileToLoad = "staging_projects.yaml"
    exists = os.path.isfile(fileToLoad)
    if not exists:
        createEmptyYaml(fileToLoad)

    isWritable = os.access(fileToLoad, os.W_OK)
    if not isWritable:
        print("Unable to write to file")
        exit(1)
    with open(fileToLoad, 'r') as yamlFile:
        yamlContent = yaml.safe_load(yamlFile)
        return [yamlContent, fileToLoad]

def saveYaml(content, fileToSave):
    with open(fileToSave, 'w') as yamlFile:
        yaml.dump(content, yamlFile) 


def addProject(projectName,endPoint,owner):
    yamlData = loadYaml()
    fileContent = yamlData[0]
    fileName = yamlData[1]
    projects = fileContent[rootElement]
    if projects is None:
        fileContent = {}
        fileContent[rootElement] = []
        projects = fileContent[rootElement]
    if projects:
        for pair in projects:
            project_list+=pair["name"].lower() 
        #     if pair["name"].lower() == projectName.lower() and pair["endpoint"] == endPoint:
        #         print("Project named:" + pair["name"] + " already exists")
        #         return
    print("Project list:" + project_list)
    
    endNamePair = {}
    endNamePair["name"] = projectName
    endNamePair["endpoint"] = endPoint
    endNamePair["owner"] = owner
    projects.append(endNamePair)
    saveYaml(fileContent, fileName)

 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--endpoint", type=str, help="Provide project endpoint")
    parser.add_argument("-a", "--add", type=str, help="Provide project name to add")
    parser.add_argument("-r", "--remove", type=str, help="Provide project name to remove")
    parser.add_argument("-o", "--owner", type=str, help="Provide owner mail")
    args = parser.parse_args()

    if args.endpoint:
        endPoint = args.endpoint
    else:
        endPoint = defaultEndpoint

    if args.add:
        projectName = args.add
        owner = args.owner
        addProject(projectName,endPoint,owner)
    elif args.remove:
        projectName = args.remove
        owner = args.owner
        removeProject(projectName,endPoint,owner)