import jenkins.model.*
import hudson.model.*
 
def execute() {
    println 'Test'
    def ret = sh(script: 'uname', returnStdout: true)
    println ret
    def String influxCode = sh(script: """curl --request POST \
    https://us-east-1-1.aws.cloud2.influxdata.com/api/v2/query?org=4721c7445d1207f1  \
  --header 'Authorization: Token 3aMqykr8W7kH5nSK35Gzpk8lQ_qCW0TV2BytlOEPwM0Cdo4tSPXBjM9OaBsykjqJDnCoxEwCYNNnlNUI86lUVg==' \
  --header 'Accept: application/csv' \
  --header 'Content-type: application/vnd.flux' \
  --data 'from(bucket:"cbs_data")
        |> range(start: -30d)
        |> filter(fn: (r) => r._measurement == "airSensors")
        |> aggregateWindow(every: 1h, fn: mean)'""", returnStdout: true)
    println influxCode
}

return this
