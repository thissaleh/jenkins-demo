#!/usr/bin/python

import argparse
import os
import sys
import yaml
import csv
import json

project_list = ''
rootElement = "projects"
yamlContent = None
endPoint = None
defaultEndpoint = "bitbk"


def loadCsvString(activeProjects):
    rows = []

    data = activeProjects  # Send HTTP GET request via requests
    data2 = """,result,table,_start,_stop,_time,_value,_field,_measurement,sensor_id
    ,_result,project1,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T18:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project2,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T19:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project3,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T20:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project4,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T21:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project5,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T22:00:00Z,,humidity,airSensors,TLM0100"""
    data3 = """,result,table,_start,_stop,_time,_value,_field,_measurement,sensor_id
    ,_result,project1,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T18:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project2,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T19:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project3,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T20:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project4,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T21:00:00Z,,humidity,airSensors,TLM0100
    ,_result,project5,2022-02-24T17:33:34.987018118Z,2022-03-26T17:33:34.987018118Z,2022-02-24T22:00:00Z,,humidity,airSensors,TLM0100"""

    # Convert to iterator by splitting on \n chars
    #lines = data.text.splitlines()
    # Parse as CSV object
    csvreader = csv.reader(data.split('\n'), delimiter=',')
    header = next(csvreader)
    for row in csvreader:
        rows.append(row)
    # print(header[2])
    # print(rows[2][2])
    return rows


def loadCsv():
    rows = []
    with open("ActiveProjects.csv", 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        for row in csvreader:
            rows.append(row)
    # print(header[2])
    # print(rows[1][2])
    return rows


def loadJson(activeProjects):
    # some JSON:
    x = """{
    "results": [
        {
            "statement_id": 0,
            "series": [
                {
                    "name": "queuedtime",
                    "tags": {
                        "project": "test-project-group"
                    },
                    "columns": [
                        "time",
                        "count"
                    ],
                    "values": [
                        [
                            "2021-10-11T09:41:28.319Z",
                            8
                        ]
                    ]
                },
                {
                    "name": "queuedtime",
                    "tags": {
                        "project": "vanderr8"
                    },
                    "columns": [
                        "time",
                        "count"
                    ],
                    "values": [
                        [
                            "2021-10-11T09:41:28.319Z",
                            7
                        ]
                    ]
                }
            ]
        }
    ]
    }"""

    # parse x:
    # projects = json.loads(x)
    projects = json.loads(activeProjects)
    projectList = []
    if projects:
        for pair in projects["results"][0]["series"]:
            projectList.append(pair["tags"]["project"].lower())
    print("Active Projects:\n" + ', '.join(projectList))
    return projectList
    # the result is a Python dictionary:
    # print(y["results"][0]["series"][0]["tags"]["project"])
    # print(y["results"][0]["series"][0]["values"][0][1])


def loadYaml():
    fileToLoad = None
    projectList = []
    # jenkinsUrl = os.environ['JENKINS_URL']
    cbsProdUrl = "http://cbs.kubemea.science.roche.com/"

    fileToLoad = "projects.yaml"
    exists = os.path.isfile(fileToLoad)
    if not exists:
        print("projects.yaml does not exist")

    isWritable = os.access(fileToLoad, os.W_OK)
    if not isWritable:
        print("Unable to write to file")
        exit(1)

    with open(fileToLoad, 'r') as yamlFile:
        yamlContent = yaml.safe_load(yamlFile)

    yamlFileContent = yamlContent
    fileName = fileToLoad
    projects = yamlFileContent[rootElement]
    if projects is None:
        yamlFileContent = {}
        yamlFileContent[rootElement] = []
        projects = yamlFileContent[rootElement]
    if projects:
        for pair in projects:
            projectList.append(pair["name"].lower())
        #     if pair["name"].lower() == projectName.lower() and pair["endpoint"] == endPoint:
        #         print("Project named:" + pair["name"] + " already exists")
        #         return
    #print("Project list:" +'\n'+ projectList[4])
    return projectList


def readProjects(activeProjects):

    # csvData = loadCsvString(activeProjects)
    jsonData = loadJson(activeProjects)
    #csvData = loadCsv()
    yamlData = loadYaml()
    # Driver Code
    li1 = jsonData  # [1, 2, 3, 4, 5]
    # print("li1:\n" + ', '.join(li1))
    # li1 = csvData # [[1,1],[3,2],[1,3],[3,4],[1,5],[1,6]]
    li2 = yamlData  # [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # print("li2:\n" + ', '.join(li2))
    # li3 = [c for a, b, c, d, e, v, j, h, i, g in csvData]
    # UnactiveProjects = Diff(yamlData, li3)
    UnactiveProjects = Diff(jsonData, yamlData)
    # print(UnactiveProjects)
    # print(yamlData)
    print("\nAbandoned Projects:\n" + ', '.join(UnactiveProjects))
    return UnactiveProjects

# Using set()


def Diff(first, second):
    diff = list(set(second).difference(set(first)))
    return diff


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--add", type=str,
                        help="Provide project name to add")
    args = parser.parse_args()

    if args.add:
        X = """{
            "results": [
                {
            "statement_id": 0,
            "series": [
                {
                    "name": "queuedtime",
                    "tags": {
                        "project": "test-project-group"
                    },
                    "columns": [
                        "time",
                        "count"
                    ],
                    "values": [
                        [
                            "2021-10-11T09:41:28.319Z",
                            8
                        ]
                    ]
                },
                {
                    "name": "queuedtime",
                    "tags": {
                        "project": "vanderr8"
                    },
                    "columns": [
                        "time",
                        "count"
                    ],
                    "values": [
                        [
                            "2021-10-11T09:41:28.319Z",
                            7
                        ]
                    ]
                    }
                  ]
                  }
            ]
             }"""
        projectName = args.add
        readProjects(projectName)
        # loadCsv()
